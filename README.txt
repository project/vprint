
-- SUMMARY --

The VPrint module will create an image (using the GD library) that prints text
virtically. The text and font size are specified as GET parameters. You can also
optionally specify the maximum vertical size, in pixels.

Usage:
www.example.com/vprint?text=This is a test&size=30&maxsize=400

Limitations:
Currently only supports one font - the open source "Bitstream Vera Sans", though
this is easy to change.

For a full description of the module, visit the project page:
  http://drupal.org/project/vprint

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/vprint

-- INSTALLATION --

Nothing special - install, enable & go.
